# Frontend Programming Challenge

It's up to you to choose a framework/library or use vanilla JS, whatever you feel most comfortable with.

## Challenge

Build a UI which lets the user search through the [Art Institute of Chicago](https://www.artic.edu/) artwork collection.

### Features

- Search query input field
- Submit button to retrieve results for the provided query
- List of artworks
- Target browser is Google Chrome (latest version)

### Nice to haves

- +1 for adding tests
- +1 for configuring code linting
- +1 for providing a way to paginate through results
- +1 for limiting json payload to displayed data

## Resources

- [Art Institute of Chicago API Docs](https://api.artic.edu/docs/#introduction)

## Submission

Put your submission on your GitHub, GitLab, Bitbucket, etc. account as a public repository and send the link back to us.

Good luck! :)
